const moment = require('moment');
const queries = require('./queries/queries.js');
const aggregator = require('./services/events-aggregator.js');
const autostart = require('./services/autostart.js');

const today = async (req, res) => {
  const events = await queries.today();
  res.status(200).json(events);
};

const calendar = async (req, res) => {
  const dates = await queries.calendar();
  res.status(200).json(dates);
};

const day = async (req, res) => {
  const date = moment(req.params.date);
  const start = date.startOf('day').toDate();
  const end = date.endOf('day').toDate();

  const events = await queries.between(start, end);
  res.status(200).json(events);
};

const manual = async (req, res) => {
  await aggregator.add('manual-event');
  res.status(200).end();
};

const getAutostart = async (req, res) => {
  const isEnabled = await autostart.isEnabled();
  res.status(200).json(isEnabled);
};

const setAutostart = (req, res) => {
  const flag = req.params.flag;
  autostart.setEnabled(flag === 'true');
  res.status(200).end();
};

module.exports = {
  autostart: {
    get: getAutostart,
    set: setAutostart
  },
  today,
  calendar,
  day,
  manual
};
