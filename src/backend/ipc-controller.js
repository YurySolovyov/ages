const state = new Map([
  ['port', 0]
]);

const port = (req, res) => {
  res.status(200).json(state.get('port'));
};

const maybeSetPort = address => {
  if (address.port) {
    state.set('port', address.port);
  }
};

module.exports = {
  port,
  maybeSetPort
};
