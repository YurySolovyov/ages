const queries = require('../queries/queries.js');
const aggregator = require('./events-aggregator.js');

const start = async () => {
  const todaysEvents = await queries.today();
  const hasFirstRunEvent = todaysEvents.some(event => {
    return event.type === 'first-run';
  });

  if (!hasFirstRunEvent) {
    aggregator.add('first-run');
  }
};

module.exports = { start };
