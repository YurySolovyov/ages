const dbus = require('dbus-native');

const aggregator = require('./events-aggregator');

const desktops = new Map();

desktops.set('unity', {
  name: 'com.canonical.Unity',
  path: '/com/canonical/Unity/Session',
  namespace: 'com.canonical.Unity.Session',
  listen(iface) {
    iface.on('Locked',() => {
      aggregator.add('screen-lock');
    });

    iface.on('Unlocked', () => {
      aggregator.add('screen-unlock');
    });
  }
});

desktops.set('cinnamon', {
  name: 'org.cinnamon.ScreenSaver',
  path: '/org/cinnamon/ScreenSaver',
  namespace: 'org.cinnamon.ScreenSaver',
  listen(iface) {
    iface.on('ActiveChanged', locked => {
      aggregator.add(locked ? 'screen-lock' : 'screen-unlock');
    });
  }
});

// very general fallback, should always be last
desktops.set('freedesktop', {
  name: 'org.freedesktop.ScreenSaver',
  path: '/org/freedesktop/ScreenSaver',
  namespace: 'org.freedesktop.ScreenSaver',
  listen(iface) {
    iface.on('ActiveChanged', locked => {
      aggregator.add(locked ? 'screen-lock' : 'screen-unlock');
    });
  }
});

const tryGetInterface = desktop => {
  const bus = dbus.sessionBus();
  const service = bus.getService(desktop.name);
  return new Promise((resolve, reject) => {
    service.getInterface(desktop.path, desktop.namespace, (err, iface) => {
      if (err) {
        return reject(err);
      }
      resolve(iface);
    });
  });
};

const start = async () => {
  for (const [name, desktop] of desktops) {
    try {
      const iface = await tryGetInterface(desktop);
      desktop.listen(iface);
      console.warn(`Succeeded listening to '${name}' desktop on DBus.`);
      break;
    } catch (e) {
      // Meh.
      console.warn(`Listening to '${name}' desktop on DBus failed.`);
    }
  }
};

module.exports = { start };
