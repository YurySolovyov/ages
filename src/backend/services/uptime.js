const moment = require('moment');
const execa = require('execa');

const queries = require('../queries/queries.js');
const aggregator = require('./events-aggregator.js');

const command = 'last reboot --time-format iso --nohostname';
const headers = [
  'type',
  'user',
  'event',
  'start',
  'end',
  'total',
];

const separator = /\s[\s-]*/;

const parseEventLine = line => {
  return line.split(separator).reduce((obj, segment, index) => {
    const key = headers[index];
    return Object.assign(obj, { [key]: segment });
  }, {});
};

const maybeParseTime = event => {
  const startMoment = moment(event.start);
  if (startMoment.isValid()) {
    event.start = startMoment;
  }

  if (event.end === 'still' || event.total === 'running') {
    return event;
  }

  const endMoment = moment(event.end);
  if (endMoment.isValid()) {
    event.end = endMoment;
  }
  return event;
};

const linesFromOutput = output => {
  return output.split(/\r?\n/).filter(line => line.length > 0 && !line.startsWith('wtmp'));
};

const addUptimeEvent = record => {
  return aggregator.add('uptime-entry', record.event.start.toDate());
};

const runUptimeAndSave = async () => {
  const output = await execa.shell(command);
  const events = linesFromOutput(output.stdout).map(parseEventLine).map(maybeParseTime);
  const eventsInDb = events.map(event => {
    return queries
      .exactlyAt(event.start.toDate())
      .then(record => {
        return { record, event };
      });
  });
  const eventsPresence = await Promise.all(eventsInDb);
  const newEvents = eventsPresence.filter(e => e.record === undefined);
  return Promise.all(newEvents.map(addUptimeEvent));
};

const start = async () => {
  return runUptimeAndSave();
};

module.exports = { start };
