const aggregator = require('./events-aggregator.js');

let interval = null;

const start = () => {
  interval = setInterval(() => { aggregator.add('time-tick'); }, 60 * 1000);
};

const stop = () => {
  clearInterval(interval);
};

module.exports = { start, stop };
