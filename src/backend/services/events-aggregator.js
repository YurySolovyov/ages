const os = require('os');

const queries = require('../queries/queries.js');

const add = (type, timestamp = Date.now()) => {
  return queries.insertTick({
    user: os.userInfo().username,
    type,
    timestamp
  });
};

module.exports = { add };
