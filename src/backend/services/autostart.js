const AutoLaunch = require('auto-launch');

const launcherStub = {
  flag: false,

  enable() {
    this.flag = true;
  },

  disable() {
    this.flag = false;
  },

  isEnabled() {
    return Promise.resolve(this.flag);
  }
};

let launcher;

const start = () => {
  launcher = process.pkg === undefined ? launcherStub : new AutoLaunch({
  	name: 'Ages',
  	path: process.execPath,
  });
};

const setEnabled = flag => {
  if (flag === true) {
    launcher.enable();
  } else {
    launcher.disable();
  }
};

const isEnabled = () => {
  return launcher.isEnabled();
};


module.exports = { start, setEnabled, isEnabled };
