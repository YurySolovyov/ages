const moment = require('moment');
const util = require('util');

const db = require('../db.js');

const run = util.promisify(db.run.bind(db));
const all = util.promisify(db.all.bind(db));
const get = util.promisify(db.get.bind(db));

const bootstrap = () => {
  return run(`create table if not exists events (id integer primary key,
                                                 user text not null,
                                                 type text not null,
                                                 timestamp date not null);`);
};

const calendar = () => {
  return all("select distinct date(timestamp / 1000, 'unixepoch') as day from events order by timestamp;");
};

const exactlyAt = time => {
  return get('select * from events where timestamp = ?;', time);
};

const today = () => {
  const todayMoment = moment({ hours: 0 }).toDate();
  return all('select * from events where timestamp > ? order by timestamp;', todayMoment);
};

const between = (start, end) => {
  return all('select * from events where timestamp > ? and timestamp < ? order by timestamp;', [start, end]);
};

const insertTick = params => {
  const { user, type, timestamp } = params;
  return run('insert into events(user, type, timestamp) values (?, ?, ?);', [user, type, timestamp]);
};

module.exports = {
  bootstrap,
  calendar,
  exactlyAt,
  insertTick,
  today,
  between
};
