const path = require('path');
const express = require('express');
const api = express();

const apiActions = require('./api-controller.js');
const ipcActions = require('./ipc-controller.js');

const root = path.resolve(__dirname, '../../');
const configPath = path.join(root, 'config.ages.js');
const config = require(configPath);

const staticDir = path.join(root, 'static');
api.use(express.static(staticDir));

api.get('/api/today', apiActions.today);
api.get('/api/calendar', apiActions.calendar);
api.get('/api/day/:date', apiActions.day);

api.post('/api/manual-event', apiActions.manual);

api.get('/api/autostart', apiActions.autostart.get);
api.post('/api/autostart/:flag', apiActions.autostart.set);

const ipc = express();

ipc.get('/port', ipcActions.port);

const listenApp = (application, address) => {
  return new Promise(resolve => {
    const server = application.listen(address);
    server.on('listening', () => {
      const addr = server.address();
      ipcActions.maybeSetPort(addr);
      resolve(addr);
    });
  });
};

module.exports = options => {
  return Promise.all([
    listenApp(api, options.dev ? 9000 : 0),
    listenApp(ipc, config.socket)
  ]);
};
