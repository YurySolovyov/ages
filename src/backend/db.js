const os      = require('os');
const path    = require('path');
const touch   = require('touch');
const mkdirp  = require('mkdirp');
const sqlite3 = require('sqlite3').verbose();

const dbPath  = path.join(os.homedir(), '.ages', 'db.sqlite3');

mkdirp.sync(path.dirname(dbPath));
touch.sync(dbPath);

module.exports = new sqlite3.Database(dbPath);
