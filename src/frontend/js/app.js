const React = require('react');
const { connect } = require('react-redux');

const SplashScreen = require('./components/splash-screen.js');
const EventsList = require('./components/events-list.js');
const TimeRing = require('./components/time-ring.js');

class App extends React.Component {
  render() {
    // TODO: only show on initial load?
    // if (this.props.ticks.currentTicks.length === 0) {
    //   return <SplashScreen />;
    // };

    return (
      <React.Fragment>
        <TimeRing events={this.props.ticks.currentTicks}
                  date={this.props.ticks.currentDate}
                  calendar={this.props.ticks.calendar}
                  dayStartingEvent={this.props.ticks.dayStartingEvent} />
        <EventsList events={this.props.ticks.currentTicks}
                    date={this.props.ticks.currentDate}
                    dayStartingEvent={this.props.ticks.dayStartingEvent}
                    autostart={this.props.autostart.autostart} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    ticks: state.ticks,
    autostart: state.autostart
  };
}

module.exports = connect(mapStateToProps)(App);
