module.exports = {
  time: {
    short: 'HH:mm',
  },
  date: {
    full: 'DD MMM YYYY, HH:mm',
    short: 'DD.MM.YYYY',
    picker: 'YYYY-MM-DD'
  }
};
