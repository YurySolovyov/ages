const moment = require('moment');

const firstAndLastOf = items => {
  return [
    items[0],
    items[items.length - 1]
  ];
};

const firstByTypeAndLast = (items, type) => {
  return [
    items.find(item => item.type === type) || items[0],
    items[items.length - 1]
  ];
};

const minutesDifference = (events, eventType = 'time-tick') => {
  const [first, last] = firstByTypeAndLast(events, eventType).map(event => moment(event.timestamp));
  return moment.duration(last.diff(first)).asMinutes();
};

const partitionBy = (collection, predicate) => {
  let currentValue = predicate(collection[0]);
  return collection.reduce((acc, item) => {
    const nextValue = predicate(item);
    if (currentValue === nextValue && (acc.length > 0)) {
      acc[acc.length - 1].push(item);
    } else {
      acc.push([item]);
    }
    currentValue = nextValue;
    return acc;
  }, []);
};

const durationVerbose = duration => {
  const hours = duration.hours();
  const minutes = duration.minutes();
  if (hours === 0) {
    return `${ minutes } minutes`;
  } else {
    return `${ hours } hours ${ minutes } minutes`;
  }
};

const durationShort = duration => {
  const hours = duration.hours();
  const minutes = duration.minutes();
  return `${ hours }:${ minutes.toString().padStart(2, '0') }`;
}

module.exports = {
  durationShort,
  durationVerbose,
  firstAndLastOf,
  minutesDifference,
  partitionBy,
};
