const { combineReducers } = require('redux');
const ticks = require('./ticks.js');
const autostart = require('./autostart.js');

module.exports = combineReducers({ ticks, autostart });
