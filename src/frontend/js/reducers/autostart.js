const match = require('pamach');

const imtbl = require('imtbl/dist/imtbl.js');
const initial = require('./initial-state.js');

module.exports = (state, action) => {
  return match(action.type)({
    'set-autostart': () => {
      return imtbl.assoc(state, 'autostart', action.autostart);
    },
    'default': () => { return state || initial; }
  });
};
