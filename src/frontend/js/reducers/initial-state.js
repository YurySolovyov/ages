const TicksService = require('../services/ticks.js');

module.exports = {
  dayStartingEvent: TicksService.ensureAndFetchStartingEvent(),
  calendar: [],
  currentDate: 'today',
  currentTicks: [],
  autostart: false
};
