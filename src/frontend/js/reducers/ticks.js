const match = require('pamach');

const imtbl = require('imtbl/dist/imtbl.js');
const initial = require('./initial-state.js');

module.exports = (state, action) => {
  return match(action.type)({
    'set-day-starting-event': () => {
      return imtbl.assoc(state, 'dayStartingEvent', action.eventType);
    },
    'set-calendar': () => {
      return imtbl.assoc(state, 'calendar', action.dates);
    },
    'set-ticks': () => {
      return imtbl.assoc(state, 'currentTicks', action.ticks);
    },
    'set-current-date': () => {
      return imtbl.assoc(state, 'currentDate', action.date);
    },
    'default': () => { return state || initial; }
  });
};
