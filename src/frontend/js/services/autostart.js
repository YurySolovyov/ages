const fetchAutostart = async () => {
  return await fetch('/api/autostart').then(res => res.json());
};

const saveAutostart = flag => {
  return fetch(`/api/autostart/${flag}`, { method: 'POST' });
};

module.exports = { fetchAutostart, saveAutostart };
