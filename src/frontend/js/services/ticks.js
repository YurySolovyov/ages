const moment = require('moment');

const fetchToday = () => {
  return fetch('/api/today').then(res => res.json());
};

const fetchCalendar = () => {
  return fetch('/api/calendar').then(res => res.json());
};

const fetchDate = date => {
  const formatted = date.format('YYYY-MM-DD');
  return fetch(`/api/day/${formatted}`).then(res => res.json());
};

const sendManualEvent = () => {
  return fetch('/api/manual-event', { method: 'POST' });
};

const saveDayStaringEvent = type => {
  localStorage.setItem('dayStartingEvent', type);
};

const ensureAndFetchStartingEvent = () => {
  const defaultType = 'time-tick';
  if (localStorage.getItem('dayStartingEvent') === null) {
    localStorage.setItem('dayStartingEvent', defaultType);
  }
  return localStorage.getItem('dayStartingEvent');
};

module.exports = {
  fetchCalendar,
  fetchDate,
  fetchToday,
  sendManualEvent,
  ensureAndFetchStartingEvent,
  saveDayStaringEvent
};
