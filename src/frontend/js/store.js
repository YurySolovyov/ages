const redux = require('redux');
const thunk = require('redux-thunk').default;

const rootReducer = require('./reducers/index.js');

module.exports = redux.createStore(rootReducer, redux.applyMiddleware(thunk));
