const React = require('react');
const moment = require('moment');

const TimeRingControls = require('./time-ring-controls.js');

const utils = require('../utils/utils.js');

class TimeRing extends React.Component {
  timePassed(events) {
    const type = this.props.dayStartingEvent;
    return utils.minutesDifference(events, type);
  }

  timeDigits(events) {
    const minutes = this.timePassed(events);
    return utils.durationShort(moment.duration(minutes, 'minutes'));
  }

  ticks() {
    if (this.props.events.length === 0) {
      return [];
    }

    const passed = this.timePassed(this.props.events);
    const total = moment.duration(8, 'hours').asMinutes();
    const fillRingTicksCount = 180;
    const degPerTick = 360 / fillRingTicksCount;
    const ticksPassed = Math.floor(fillRingTicksCount * (passed / total));

    const ticks = [];
    for (let i = 0; i < ticksPassed; i++) {
      const ringIndex = Math.floor(i / fillRingTicksCount);
      ticks.push({
        deg: i * degPerTick,
        id: i,
        type: ringIndex === 0 ? 'normal' : 'overtime',
        ringIndex
      });
    }
    return ticks;
  }

  render() {
    return (
      <div className="time-rings col col-6 mt2">
        <TimeRingControls date={this.props.date} calendar={this.props.calendar} />
        <svg width="600px" height="600px" viewBox="0 0 600 600" className="time-ring mx-auto block">
          <g transform="translate(300 300) rotate(-90)">
            {
              this.ticks().map(tick => {
                const base = 210;
                const lineSize = 30;
                const linesGap = 10;
                const offset = (lineSize + linesGap * 2) * tick.ringIndex;

                const start = base + offset;
                const end = base + lineSize + linesGap + offset;
                return <line x1={start}
                             x2={end}
                             key={tick.id}
                             className={tick.type}
                             style={{
                              transform: `rotate(${ tick.deg }deg)`,
                              animationDelay: `${tick.id * 3}ms`
                             }} />;
              })
            }
          </g>
        </svg>
        <div className="time-digits center">
          {this.props.events.length > 0 ? this.timeDigits(this.props.events) : "Nothin'"}
        </div>
      </div>
    );
  }
};

module.exports = TimeRing;
