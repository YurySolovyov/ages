const React = require('react');
const moment = require('moment');

const formats = require('../utils/formats.js');

class SingleEvent extends React.Component {
  timeFormatted(event) {
    return moment(event.timestamp).format(formats.time.short);
  }

  render() {
    const event = this.props.events[0];
    return (
      <div className="single-event">
        <span className="list-cell inline-block col-3">{event.type}</span>
        <span className="list-cell inline-block col-3">{this.timeFormatted(event)}</span>
      </div>
    );
  }
}

module.exports = SingleEvent;
