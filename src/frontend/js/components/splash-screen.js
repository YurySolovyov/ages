const React = require('react');

const AgesIcon = require('./ages-icon.js');

const SplashScreen = () => {
  return (
    <div className="splash-screen center">
      <AgesIcon />
      <span>Ages</span>
    </div>
  );
}

module.exports = SplashScreen;
