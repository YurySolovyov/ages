const React = require('react');
const classnames = require('classnames');
const moment = require('moment');

const utils = require('../utils/utils.js');
const formats = require('../utils/formats.js');

class EventsGroup extends React.Component {
  constructor() {
    super();

    this.state = { expanded: false };
  }

  toggle() {
    this.setState({ expanded: !this.state.expanded });
  }

  totalTime(events) {
    const minutes = utils.minutesDifference(events);
    return utils.durationVerbose(moment.duration(minutes, 'minutes'));
  }

  timeFormatted(event) {
    return moment(event.timestamp).format(formats.date.full);
  }

  fromToLabel(events) {
    const [first, last] = utils.firstAndLastOf(events).map(event => moment(event.timestamp));
    return `${ first.format(formats.time.short) } — ${ last.format(formats.time.short) }`;
  }

  render() {
    return (
      <div className="events-group">
        <div onClick={this.toggle.bind(this)}>
          <span className="list-cell inline-block col-3">{this.props.events[0].type}</span>
          <span className="list-cell inline-block col-2">{this.fromToLabel(this.props.events)}</span>
          <span className="list-cell inline-block col-7">({this.totalTime(this.props.events)})</span>
        </div>
        <div className={ classnames({ hide: !this.state.expanded }) }>
          <span className="list-cell bold inline-block col-3">User</span>
          <span className="list-cell bold inline-block col-2">Event</span>
          <span className="list-cell bold inline-block col-7">Date</span>
          {
            this.props.events.map(event => {
              return <div className="event-item" key={event.id}>
                <span className="list-cell raw inline-block col-3">{event.user}</span>
                <span className="list-cell inline-block col-2">{event.type}</span>
                <span className="list-cell inline-block col-7">{this.timeFormatted(event)}</span>
              </div>
            })
          }
        </div>
      </div>
    );
  }
}

module.exports = EventsGroup;
