const React = require('react');
const classnames = require('classnames');

const ticks = require('../actions/ticks.js');
const autostart = require('../actions/autostart.js');
const store = require('../store.js');

class EventsListControls extends React.Component {
  onEventTypeSelect(type) {
    store.dispatch(ticks.setDayStartingEvent(type));
  }

  onManualEventClick() {
    store.dispatch(ticks.sendManualEvent());
  }

  onAutostartToggle() {
    const flag = !this.props.autostart;
    store.dispatch(autostart.saveAutostart(flag));
  }

  eventTypeButtom(type) {
    const classes = classnames('inline-block', {
      active: this.props.startEvent === type
    });

    return (
      <span className={classes} onClick={this.onEventTypeSelect.bind(this, type)}>{ type }</span>
    );
  }

  render() {
    const manualEventClasses = classnames('manual-event-submit', 'inline-block', 'right', 'my1', {
      hide: this.props.date !== 'today'
    });

    const autostartButtonClasses = classnames('autostart-toggle', 'inline-block', 'right', 'my1', {
      on: this.props.autostart
    });

    return (
      <div className="events-list-controls overflow-hidden">
        <span className="inline-block py2">Day starts with first</span>

        <span className="inline-block event-type-selector m1">
          { this.eventTypeButtom('time-tick') }
          { this.eventTypeButtom('screen-unlock') }
          { this.eventTypeButtom('uptime-entry') }
        </span>

        <span className={autostartButtonClasses} onClick={this.onAutostartToggle.bind(this)}>
          Auto-start is { this.props.autostart ? 'on' : 'off' }
        </span>
        <span className={manualEventClasses} onClick={this.onManualEventClick}>Add manual event</span>
      </div>
    );
  }
}


module.exports = EventsListControls;
