const React = require('react');

const EventsGroup = require('./events-group.js');
const SingleEvent = require('./single-event.js');
const EventsListControls = require('./events-list-controls.js');

const utils = require('../utils/utils.js');

const idRange = events => {
  const [first, last] = utils.firstAndLastOf(events);
  return `${first.id}-${last.id}`;
};

const EventsList = (props) => {
  if (props.events.length === 0) {
    return null;
  }

  const groups = utils.partitionBy(props.events, item => item.type);
  return (
    <div id="events-list" className="col col-6 mt2 mxn2 px2">
      <EventsListControls startEvent={props.dayStartingEvent} date={props.date} autostart={props.autostart} />
      {
        groups.map(eventsGroup => {
          const key = idRange(eventsGroup);
          if (eventsGroup.length === 1) {
            return <SingleEvent events={eventsGroup} key={key} />
          } else {
            return <EventsGroup events={eventsGroup} key={key} />
          }
        })
      }
    </div>
  );
}

module.exports = EventsList;
