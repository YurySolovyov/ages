const React = require('react');
const moment = require('moment');
const classnames = require('classnames');
const Flatpickr  = require('react-flatpickr').default;

const formats = require('../utils/formats.js');

const actions = require('../actions/ticks.js');
const store = require('../store.js');

class TimeRingControls extends React.Component {
  isToday(date = this.props.date) {
    return date === 'today' || moment().isSame(date, 'day');
  }

  getCurrentMoment() {
    return this.isToday() ? moment() : moment(this.props.date);
  }

  getPickerDate() {
    return this.getCurrentMoment().format(formats.date.picker);
  }

  hasDaysBefore() {
    const dateMoment = this.getCurrentMoment();
    return this.props.calendar.some(item => moment(item.day).isBefore(dateMoment));
  }

  getDateForDirection(direction) {
    const current = this.getCurrentMoment();
    const currentIndex = this.props.calendar.findIndex(item => moment(item.day).isSame(current, 'day'));
    const delta = direction === 'prev' ? -1 : 1;
    return moment(this.props.calendar[currentIndex + delta].day);
  }

  onDateNavClick(direction) {
    let newDate = this.getDateForDirection(direction);
    if (this.isToday(newDate)) {
      newDate = 'today';
      store.dispatch(actions.setCurrentDate(newDate));
      store.dispatch(actions.fetchTodayTicks());
    } else {
      store.dispatch(actions.fetchDate(newDate));
    }
  }

  calendarOptions() {
    return {
      time_24hr: true,
      altInput: true,
      altInputClass: 'date-input center',
      enable: this.props.calendar.map(d => new Date(d.day)),
      locale: {
        firstDayOfWeek: 1
      }
    };
  }

  onDateChange(dates) {
    const [date, ..._rest] = dates;
    store.dispatch(actions.fetchDate(moment(date)));
  }

  render() {
    // TODO: disable arrows on the edges of available dates bounds
    const nextClasses = classnames('date-nav-button', 'mx1', 'inline-block', {
      active: !this.isToday()
    });

    const prevClasses = classnames('date-nav-button', 'mx1', 'inline-block', {
      active: this.hasDaysBefore()
    });

    return (
      <div className="ring-controls mt2 mb3 center">
        <span className={prevClasses} onClick={this.onDateNavClick.bind(this, 'prev')}>&lt;</span>

        <Flatpickr options={this.calendarOptions()}
                   onChange={this.onDateChange.bind(this)}
                   value={this.getPickerDate()} />

        <span className={nextClasses} onClick={this.onDateNavClick.bind(this, 'next')}>&gt;</span>
      </div>
    );
  }
}

module.exports = TimeRingControls;
