const React = require('react')

const AgesIcon = () => {
  return (
    <svg className="ages-icon mr2" viewBox="0 0 24 24">
      <g>
        <polyline fill="none" strokeLinecap="round" strokeLinejoin="round" points="18.5,2.5 18.5,7.521 11.503,12.521 4.5,7.521 4.5,2.5"/>
        <polyline fill="none" strokeLinecap="round" strokeLinejoin="round" points="14,7.5 11.5,9.5 9,7.5"/>
        <polyline fill="none" strokeLinecap="round" strokeLinejoin="round" points="16,21 11.5,19.5 7,21"/>
        <polygon fill="none" strokeLinecap="round" strokeLinejoin="round" points="7.5,4.5 4.5,2.5 7.5,0.5 16,0.5 18.5,2.5 16,4.5"/>
        <polygon fill="none" strokeLinecap="round" strokeLinejoin="round" points="18.5,21.5 18.5,17.521 11.503,12.521 4.5,17.521 4.5,21.5 7.5,23.5 16,23.5"/>
        <line fill="none" strokeLinecap="round" strokeLinejoin="round" x1="11.503" y1="15.5" x2="11.503" y2="16.5"/>
        <line fill="none" strokeLinecap="round" strokeLinejoin="round" x1="11.503" y1="18.521" x2="11.5" y2="19.5"/>
      </g>
    </svg>
  );
};

module.exports = AgesIcon;
