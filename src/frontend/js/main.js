const React = require('react');
const ReactDOM = require('react-dom');

const { Provider } = require('react-redux');

const App = require('./app.js');
const store = require('./store.js');

const ticksActions = require('./actions/ticks.js');
const autostartActions = require('./actions/autostart.js');

require('../css/styles.css');

const onRendered = () => {
  store.dispatch(ticksActions.maybeFetchTodayTicks());
  store.dispatch(ticksActions.fetchCalendar());
  store.dispatch(autostartActions.fetchAutostart());
};

setInterval(onRendered, 60 * 1000);

const rootComponent =
  <Provider store={store}>
    <App />
  </Provider>;

ReactDOM.render(rootComponent, document.getElementById('app'), onRendered);
