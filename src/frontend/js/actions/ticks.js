const TicksService = require('../services/ticks.js');

const setTicks = ticks => {
  return { type: 'set-ticks', ticks };
};

const setCalendar = dates => {
  return { type: 'set-calendar', dates };
};

const setCurrentDate = date => {
  return { type: 'set-current-date', date };
};

const setDayStartingEvent = eventType => {
  TicksService.saveDayStaringEvent(eventType);
  return { type: 'set-day-starting-event', eventType };
};

const fetchDate = date => {
  return async dispatch => {
    const res = await TicksService.fetchDate(date);
    dispatch(setCurrentDate(date));
    dispatch(setTicks(res));
  };
};

const fetchTodayTicks = () => {
  return async dispatch => {
    const res = await TicksService.fetchToday();
    dispatch(setTicks(res));
  };
};

const fetchCalendar = () => {
  return async dispatch => {
    const res = await TicksService.fetchCalendar();
    dispatch(setCalendar(res));
  };
};

const maybeFetchTodayTicks = () => {
  return (dispatch, getState) => {
    const date = getState().ticks.currentDate;
    if (date === 'today') {
      dispatch(fetchTodayTicks());
    }
  };
};

const sendManualEvent = () => {
  return async dispatch => {
    await TicksService.sendManualEvent();
    dispatch(maybeFetchTodayTicks());
  };
};

module.exports = {
  setCurrentDate,
  setDayStartingEvent,
  setTicks,
  sendManualEvent,
  fetchCalendar,
  fetchDate,
  fetchTodayTicks,
  maybeFetchTodayTicks
};
