const AutostartService = require('../services/autostart.js');

const setAutostart = autostart => {
  return { type: 'set-autostart', autostart };
};

const fetchAutostart = () => {
  return async dispatch => {
    const autostart = await AutostartService.fetchAutostart();
    dispatch(setAutostart(autostart));
  };
};

const saveAutostart = flag => {
  return async dispatch => {
    await AutostartService.saveAutostart(flag);
    await dispatch(fetchAutostart());
  };
};

module.exports = {
  fetchAutostart,
  saveAutostart
};
