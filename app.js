const os = require('os');
const fs = require('fs');
const path = require('path');

const minimist = require('minimist');
const got = require('got');
const opn = require('opn');

const listener = require('./src/backend/listener.js');
const queries = require('./src/backend/queries/queries.js');
const autostart = require('./src/backend/services/autostart.js');
const scheduler = require('./src/backend/services/scheduler.js');
const dbus = require('./src/backend/services/dbus-watcher.js');
const uptime = require('./src/backend/services/uptime.js');
const firstRun = require('./src/backend/services/first-run-recorder.js');

const config = require('./config.ages.js');

const simpleServices = [
  autostart,
  scheduler,
  dbus,
  uptime,
  firstRun
];

if (os.type() !== 'Linux') {
  console.log('Only linux is supported so far');
  return process.exit(1);
}

const open = async () => {
  const response = await got('/port', { socketPath: config.socket, json: true });
  const port = response.body;
  const url = `http://localhost:${port}/`;
  console.log('Opening app on:', url);
  return opn(url, { wait: false });
};

const install = async () => {
  const cwd = process.cwd();
  if (process.env.PATH.includes(cwd)) {
    console.log('App is already on the PATH');
  } else {
    const command = `\nexport PATH=$PATH:${cwd}`;
    const fullAppPath = path.join(os.homedir(), '.bash_profile');
    await fs.promises.appendFile(fullAppPath, command);
    console.log(`Added "${cwd}" to the PATH`);
  }
};

const daemonize = async options => {
  const [addresses, ...rest] = await Promise.all([
    listener(options),
    queries.bootstrap()
  ]);

  const [apiAddress, ipcAddress] = addresses;

  simpleServices.map(service => service.start());

  console.log(`Server is listening on http://localhost:${apiAddress.port}/`);
};

const help = () => {
  console.log(`
    Usage:
      ages [options]

    Options:
      --open        Open Web UI in default browser
      --install     Add ages to the PATH (Uses ~/.bash_profile)
      --help        Show this help message

    Examples:

      $ ages
      $ ages --open
      $ ages --install
  `);
};

const appHandler = async options => {
  if (options.open === true) {
    await open();
  } else if (options.install === true) {
    await install();
  } else if (options.help === true) {
    return help();
  } else {
    await daemonize(options);
  }
};

appHandler(minimist(process.argv.slice(2)));
