const fs = require('fs');
const path = require('path');

const rootPath = path.resolve(__dirname, '..');
const destPath = path.join(rootPath, 'out/linux');

const files = [
  path.join(rootPath, 'node_modules/abstract-socket/build/Release/abstract_socket.node'),
  path.join(rootPath, 'node_modules/sqlite3/lib/binding/node-v64-linux-x64/node_sqlite3.node'),
  path.join(rootPath, 'node_modules/opn/xdg-open'),
];

files.forEach(filePath => {
  const fileName = path.basename(filePath);
  fs.copyFileSync(filePath, path.join(destPath, fileName));
});
