'use strict';

const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: {
    app: ['./src/frontend/js/main.js']
  },
  output: {
    path: path.join(__dirname, 'static', 'dist'),
    filename: '[name].js',
    publicPath: ''
  },
  module: {
    rules: [
      { test: /\.js/, use: 'babel-loader', exclude: /(node_modules|handlers)/ },
      { test: /\.css$/, use: ExtractTextPlugin.extract({ use: 'css-loader' }) },
      { test: /\.(woff2)([?]?.*)$/, use: 'url-loader' }
    ]
  },
  plugins: [
    new ExtractTextPlugin({ filename: 'styles.css', allChunks: true }),
  ],
  resolve: {
    alias: {
      fonts: path.resolve(__dirname, 'static/fonts')
    }
  }
};
